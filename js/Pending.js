"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gSTT = 1;
const gBASE_URL = "http://localhost:8080/";
var gAddressTable = $("#pending-estate-table").DataTable({
    "responsive": true, "lengthChange": false, "autoWidth": false,
    "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
    "scrollX": true,
    "columns": [
        { data: 'id' },
        { data: 'address' },
        { data: 'type' },
        { data: 'request' },
        { data: 'customerId' },
        { data: 'price' },
        { data: 'dateCreate' },
        { data: 'acreage' },
        { data: 'totalFloors' },
        { data: 'longX' },
        { data: 'widthY' },
        {
            data: "photo",
            render: function (data) {
                return '<img src="data:image/jpeg;base64,' + data + '" alt="Photo" width="100" height="100">';
            }
        },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 0, // Cột số thứ tự là 0 (cột STT)
            render: function () {
                return gSTT++;
            }
        },
        {
            targets: 2,
            render: function changeType(data) {
                if (data == 0) {
                    return "Land";
                }
                else if (data == 1) {
                    return "Home"
                }
                else if (data == 2) {
                    return "Apartment";
                }
                else if (data == 3) {
                    return "Office";
                }
                else if (data == 4) {
                    return "Bussiness";
                }
                else if (data == 5) {
                    return "Motel";
                }
            }
        },
        {
            targets: 3,
            render: function changeType(data) {
                if (data == 0) {
                    return "For Sale";
                }
                else if (data == 1) {
                    return "Need To Purcharse"
                }
                else if (data == 2) {
                    return "For Rent";
                }
                else if (data == 3) {
                    return "Need To Rent";
                }

            }
        },
        {
            targets: -1,
            defaultContent: '<i class="fas fa-edit mr-3 btn-update"></i>' + '<i class="fas fa-trash-alt btn-delete"></i>'
        }
    ]
}).buttons().container().appendTo('#pending-estate-table_wrapper .col-md-6:eq(0)');
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    $(".btn-log-out").on("click", function () {
        setCookie("token", "", 1);
        window.location.href = "http://127.0.0.1:80/Admin_Dashboard/Login.html";
    })

    $("#pending-estate-table").on("click", ".btn-update", function () {
        $("#modal-up").modal('show');
        var vData = getDataRowTable(this);
        fillDataUpdate(vData);
        $("#btn-save-up").on("click", function () {
            onBtnUp(vData.id);
        })
    })
    $("#pending-estate-table").on("click", ".btn-delete", function () {
        $("#modal-delete").modal('show');
        var vData = getDataRowTable(this);
        $("#btn-save-delete").on("click", function () {
            onBtnDelete(vData.id);
        })
    })
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    $.ajax({
        url: gBASE_URL + "real-estates/pending",
        type: "GET",
        headers: { "Authorization": "Bearer " + getCookie("token") },
        success: function (paramRes) { 
            drawTable(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
function onBtnUp(paramId) {
    var vObj = {
        address: "",
        type: "",
        request: "",
        customerId: "",
        price: "",
        dateCreate: "",
        acreage: "",
        totalFloors: "",
        longX: "",
        widthY: "",
        photo: "",
        proccess: "active",
    }
    getInfoUpdate(vObj);
    $.ajax({
        url: gBASE_URL + "real-estates/" + paramId,
        type: "PUT",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(vObj),
        success: function () {
            alert("Cập nhật dữ liệu thành công!");
            location.reload();
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}

function onBtnDelete(paramId) {
    $.ajax({
        url: gBASE_URL + "real-estates/" + paramId,
        type: "DELETE",
        success: function () {
            alert("Xóa dữ liệu thành công");
            location.reload();
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function drawTable(paramData) {
    gSTT = 1;
    var vTable = $("#pending-estate-table").DataTable();
    vTable.clear();
    vTable.rows.add(paramData);
    vTable.draw();
}

function getInfo(paramObj) {
    paramObj.address = $("#inp-add-address").val();
    paramObj.type = $("#inp-add-type").val();
    paramObj.request = $("#inp-add-request").val();
    paramObj.customerId = $("#inp-add-customerId").val();
    paramObj.price = $("#inp-add-price").val();
    paramObj.dateCreate = $("#inp-add-dateCreate").val();
    paramObj.acreage = $("#inp-add-acreage").val();
    paramObj.totalFloors = $("#inp-add-totalFloors").val();
    paramObj.longX = $("#inp-add-longX").val();
    paramObj.widthY = $("#inp-add-widthY").val();
    paramObj.photo = $("#inp-add-photo").val();
}

function getDataRowTable(paramElement) {
    var vTable = $("#pending-estate-table").DataTable();
    var vRow = $(paramElement).closest("tr");
    var vData = vTable.row(vRow).data();
    return vData;
}
function fillDataUpdate(paramData) {
    $("#inp-update-address").val(paramData.address);
    $("#inp-update-type").val(paramData.type);
    $("#inp-update-request").val(paramData.request);
    $("#inp-update-customerId").val(paramData.customerId);
    $("#inp-update-price").val(paramData.price);
    $("#inp-update-acreage").val(paramData.acreage);
    $("#inp-update-totalFloors").val(paramData.totalFloors);
    $("#inp-update-longX").val(paramData.longX);
    $("#inp-update-widthY").val(paramData.widthY);
    $("#inp-update-photo").val(paramData.photo);
}
function getInfoUpdate(paramObj) {
    paramObj.address = $("#inp-update-address").val();
    paramObj.type = $("#inp-update-type").val();
    paramObj.request = $("#inp-update-request").val();
    paramObj.customerId = $("#inp-update-customerId").val();
    paramObj.price = $("#inp-update-price").val();
    paramObj.acreage = $("#inp-update-acreage").val();
    paramObj.totalFloors = $("#inp-update-totalFloors").val();
    paramObj.longX = $("#inp-update-longX").val();
    paramObj.widthY = $("#inp-update-widthY").val();
    paramObj.photo = $("#inp-update-photo").val();
}


//Hàm setCookie
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/Admin_Dashboard";
}

// hàm lấy cookie từ web
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//Hiển thị tên
function showUserName() {
    const data = JSON.parse(localStorage.getItem('login'));
    if (data.accessToken) {
        $("#Admin-name").text(data.username)
    } else {
        window.location.href = "http://127.0.0.1:80/Admin_Dashboard/Login.html";
    }
}
