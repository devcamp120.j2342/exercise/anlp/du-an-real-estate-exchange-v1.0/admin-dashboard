"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gSTT = 1;
const gBASE_URL = "http://localhost:8080/";
var gAddressTable = $("#employee-table").DataTable({
    "responsive": true, "lengthChange": false, "autoWidth": false,
    "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
    "scrollX": true,
    "columns": [
        { data: 'employeeID' },
        { data: 'lastName' },
        { data: 'firstName' },
        { data: 'title' },
        { data: 'titleOfCourtesy' },
        { data: 'birthDate' },
        { data: 'hireDate' },
        { data: 'address' },
        { data: 'city' },
        { data: 'postalCode' },
        { data: 'country' },
        { data: 'homePhone' },
        {
            data: "photo",
            render: function (data) {
                return '<img src="data:image/jpeg;base64,' + data + '" alt="Employee Photo" width="100" height="100">';
            }
        },
        { data: 'reportsTo' },
        { data: 'username' },
        { data: 'email' },
        { data: 'activated' },
        { data: 'userLevel' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 0, // Cột số thứ tự là 0 (cột STT)
            render: function () {
                return gSTT++;
            }
        },
        {
            targets: -1,
            defaultContent: '<i class="fas fa-edit mr-3 btn-update"></i>' + '<i class="fas fa-trash-alt btn-delete"></i>'
        }
    ]
}).buttons().container().appendTo('#employee-table_wrapper .col-md-6:eq(0)');
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    $("#btn-add").on("click", function () {
        $("#modal-add").modal('show');
        $("#btn-save-add").on("click", function () {
            onBtnAdd();
        })
    })

    $("#employee-table").on("click", ".btn-update", function () {
        $("#modal-up").modal('show');
        var vData = getDataRowTable(this);
        fillDataUpdate(vData);
        $("#btn-save-up").on("click", function () {
            onBtnUp(vData.id);
        })
    })
    $("#employee-table").on("click", ".btn-delete", function () {
        $("#modal-delete").modal('show');
        var vData = getDataRowTable(this);
        $("#btn-save-delete").on("click", function () {
            onBtnDelete(vData.id);
        })
    })
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    $.ajax({
        url: gBASE_URL + "employees",
        type: "GET",
        success: function (paramRes) {
            drawTable(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
function onBtnAdd() {
    var vObj = {
        lastName: "",
        firstName: "",
        title: "",
        titleOfCourtesy: "",
        birthDate: "",
        hireDate: "",
        address: "",
        city: "",
        postalCode: "",
        country: "",
        homePhone: "",
        photo: "",
        reportsTo: "",
        username: "",
        email: "",
        activated: "",
        userLevel: "",
    }
    getInfo(vObj);
    var vCheck = validateInfo(vObj);
    if (vCheck) {
        $.ajax({
            url: gBASE_URL + "employees",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(vObj),
            success: function (paramRes) {
                alert("Tạo mới dữ liệu thành công");
                location.reload();
            },
            error: function (paramErr) {
                alert(paramErr.status);
            }
        })
    }
}
function onBtnUp(paramId) {
    var vObj = {
        lastName: "",
        firstName: "",
        title: "",
        titleOfCourtesy: "",
        birthDate: "",
        hireDate: "",
        address: "",
        city: "",
        postalCode: "",
        country: "",
        homePhone: "",
        photo: "",
        reportsTo: "",
        username: "",
        email: "",
        activated: "",
        userLevel: "",
    }
    getInfoUpdate(vObj);
    $.ajax({
        url: gBASE_URL + "employees/" + paramId,
        type: "PUT",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(vObj),
        success: function (paramRes) {
            alert("Cập nhật dữ liệu thành công!");
            location.reload();
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}

function onBtnDelete(paramId) {
    $.ajax({
        url: gBASE_URL + "employees/" + paramId,
        type: "DELETE",
        success: function (paramRes) {
            alert("Xóa dữ liệu thành công");
            location.reload();
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function drawTable(paramData) {
    gSTT = 1;
    var vTable = $("#employee-table").DataTable();
    vTable.clear();
    vTable.rows.add(paramData);
    vTable.draw();
}

function getInfo(paramObj) {
    paramObj.lastName = $("#inp-add-lastName").val();
    paramObj.firstName = $("#inp-add-firstName").val();
    paramObj.title = $("#inp-add-title").val();
    paramObj.titleOfCourtesy = $("#inp-add-titleOfCourtesy").val();
    paramObj.birthDate = $("#inp-add-birthDate").val();
    paramObj.hireDate = $("#inp-add-hireDate").val();
    paramObj.address = $("#inp-add-address").val();
    paramObj.city = $("#inp-add-city").val();
    paramObj.postalCode = $("#inp-add-postalCode").val();
    paramObj.country = $("#inp-add-country").val();
    paramObj.homePhone = $("#inp-add-homePhone").val();
    paramObj.photo = $("#inp-add-photo").val();
    paramObj.reportsTo = $("#inp-add-reportsTo").val();
    paramObj.username = $("#inp-add-username").val();
    paramObj.email = $("#inp-add-email").val();
    paramObj.activated = $("#inp-add-activated").val();
    paramObj.userLevel = $("#inp-add-userLevel").val();
}

function validateInfo(paramObj) {
    if (paramObj.lastName == "" || paramObj.firstName == "") {
        alert("Xin hãy nhập họ và tên!");
        return false;
    } else if (paramObj.title == "") {
        alert("Xin hãy nhập Chức vụ!");
        return false;
    } else if (paramObj.birthDate == "") {
        alert("Xin hãy nhập ngày sinh!");
        return false;
    } else if (paramObj.hireDate == "") {
        alert("Xin hãy nhập ngày vào làm!");
        return false;
    } else if (paramObj.address == "") {
        alert("Xin hãy nhập địa chỉ");
        return false;
    } else if (paramObj.city == "") {
        alert("Xin hãy nhập thành phố!");
        return false;
    } else if (paramObj.postalCode == "") {
        alert("Xin hãy nhập mã bưu điện!");
        return false;
    } else if (paramObj.country == "") {
        alert("Xin hãy nhập quốc gia!");
        return false;
    } else if (paramObj.homePhone == "") {
        alert("Xin hãy nhập số điện thoại!");
        return false;
    } else if (paramObj.email == "") {
        alert("Xin hãy nhập email!");
        return false;
    }
    return true;
}

function getDataRowTable(paramElement) {
    var vTable = $("#employee-table").DataTable();
    var vRow = $(paramElement).closest("tr");
    var vData = vTable.row(vRow).data();
    return vData;
}
function fillDataUpdate(paramData) {
    $("#inp-update-lastName").val(paramData.lastName);
    $("#inp-update-firstName").val(paramData.firstName);
    $("#inp-update-title").val(paramData.title);
    $("#inp-update-titleOfCourtesy").val(paramData.titleOfCourtesy);
    $("#inp-update-birthDate").val(paramData.birthDate);
    $("#inp-update-hireDate").val(paramData.hireDate);
    $("#inp-update-address").val(paramData.address);
    $("#inp-update-city").val(paramData.city);
    $("#inp-update-postalCode").val(paramData.postalCode);
    $("#inp-update-country").val(paramData.country);
    $("#inp-update-homePhone").val(paramData.homePhone);
    $("#inp-update-photo").val(paramData.photo);
    $("#inp-update-reportsTo").val(paramData.reportsTo);
    $("#inp-update-username").val(paramData.username);
    $("#inp-update-email").val(paramData.email);
    $("#inp-update-activated").val(paramData.activated);
    $("#inp-update-userLevel").val(paramData.userLevel);
}
function getInfoUpdate(paramObj) {
    paramObj.lastName = $("#inp-update-lastName").val();
    paramObj.firstName = $("#inp-update-firstName").val();
    paramObj.title = $("#inp-update-title").val();
    paramObj.titleOfCourtesy = $("#inp-update-titleOfCourtesy").val();
    paramObj.birthDate = $("#inp-update-birthDate").val();
    paramObj.hireDate = $("#inp-update-hireDate").val();
    paramObj.address = $("#inp-update-address").val();
    paramObj.city = $("#inp-update-city").val();
    paramObj.postalCode = $("#inp-update-postalCode").val();
    paramObj.country = $("#inp-update-country").val();
    paramObj.homePhone = $("#inp-update-homePhone").val();
    paramObj.photo = $("#inp-update-photo").val();
    paramObj.reportsTo = $("#inp-update-reportsTo").val();
    paramObj.username = $("#inp-update-username").val();
    paramObj.email = $("#inp-update-email").val();
    paramObj.activated = $("#inp-update-activated").val();
    paramObj.userLevel = $("#inp-update-userLevel").val();
}
