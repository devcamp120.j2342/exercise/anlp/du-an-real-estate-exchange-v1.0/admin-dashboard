"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gSTT = 0;
const gBASE_URL = "http://localhost:8080/";
var gAddressTable = $("#ward-table").DataTable({
    "responsive": true, "lengthChange": false, "autoWidth": false,
    "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
    "scrollX": true,

    "columns": [
        { data: 'id' },
        { data: 'name' },
        { data: 'prefix' },
        { data: 'provinceId' },
        { data: 'districtId' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 0, // Cột số thứ tự là 0 (cột STT)
            render: function () {
                return gSTT++;
            }
        },
        {
            targets: -1,
            defaultContent: '<i class="fas fa-edit mr-3 btn-update"></i>' + '<i class="fas fa-trash-alt btn-delete"></i>'
        }
    ]
}).buttons().container().appendTo('#ward-table_wrapper .col-md-6:eq(0)');
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    $("#btn-add").on("click", function () {
        $("#modal-add").modal('show');
        $("#btn-save-add").on("click", function () {
            onBtnAdd();
        })
    })

    $("#ward-table").on("click", ".btn-update", function () {
        $("#modal-up").modal('show');
        var vData = getDataRowTable(this);
        fillDataUpdate(vData);
        $("#btn-save-up").on("click", function () {
            onBtnUp(vData.id);
        })
    })
    $("#ward-table").on("click", ".btn-delete", function () {
        $("#modal-delete").modal('show');
        var vData = getDataRowTable(this);
        $("#btn-save-delete").on("click", function () {
            onBtnDelete(vData.id);
        })
    })
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    $.ajax({
        url: gBASE_URL + "wards",
        type: "GET",
        success: function (paramRes) {
            drawTable(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
function onBtnAdd() {
    var vObj = {
        name: "",
        prefix: "",
        provinceId: "",
        districtId: "",
    }
    getInfo(vObj);
    var vCheck = validateInfo(vObj);
    if (vCheck) {
        $.ajax({
            url: gBASE_URL + "wards",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(vObj),
            success: function (paramRes) {
                alert("Tạo mới dữ liệu thành công");
                location.reload();
            },
            error: function (paramErr) {
                alert(paramErr.status);
            }
        })
    }
}
function onBtnUp(paramId) {
    var vObj = {
        name: "",
        prefix: "",
        provinceId: "",
        districtId: "",
    }
    getInfoUpdate(vObj);
    $.ajax({
        url: gBASE_URL + "wards/" + paramId,
        type: "PUT",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(vObj),
        success: function (paramRes) {
            alert("Cập nhật dữ liệu thành công!");
            location.reload();
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}

function onBtnDelete(paramId) {
    $.ajax({
        url: gBASE_URL + "wards/" + paramId,
        type: "DELETE",
        success: function (paramRes) {
            alert("Xóa dữ liệu thành công");
            location.reload();
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function drawTable(paramData) {
    gSTT = 1;
    var vTable = $("#ward-table").DataTable();
    vTable.clear();
    vTable.rows.add(paramData);
    vTable.draw();
}
function getInfo(paramObj) {
    paramObj.name = $("#inp-add-name").val();
    paramObj.prefix = $("#inp-add-prefix").val();
    paramObj.provinceId = $("#inp-add-provinceId").val();
    paramObj.districtId = $("#inp-add-districtId").val();

}

function validateInfo(paramObj) {
    if (paramObj.name == "") {
        alert("Xin hãy tên tỉnh thành!");
        return false;
    } else if (paramObj.prefix == "") {
        alert("Xin hãy nhập thông tin quận huyện!");
        return false;
    } else if (paramObj.provinceId == "") {
        alert("Xin hãy nhập id tỉnh thành!");
        return false;
    } else if (paramObj.districtId == "") {
        alert("Xin hãy nhập id quận huyện!");
        return false;
    }
    return true;
}

function getDataRowTable(paramElement) {
    var vTable = $("#ward-table").DataTable();
    var vRow = $(paramElement).closest("tr");
    var vData = vTable.row(vRow).data();
    return vData;
}
function fillDataUpdate(paramData) {
    $("#inp-update-name").val(paramData.name);
    $("#inp-update-prefix").val(paramData.prefix);
    $("#inp-update-provinceId").val(paramData.provinceId);
    $("#inp-update-districtId").val(paramData.districtId);
}
function getInfoUpdate(paramObj) {
    paramObj.name = $("#inp-update-name").val();
    paramObj.prefix = $("#inp-update-prefix").val();
    paramObj.provinceId = $("#inp-update-provinceId").val();
    paramObj.districtId = $("#inp-update-districtId").val();
}
