"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gSTT = 1;
const gBASE_URL = "http://localhost:8080/";
var gAddressTable = $("#project-table").DataTable({
    "responsive": true, "lengthChange": false, "autoWidth": false,
    "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
    "scrollX": true,
    "columns": [
        { data: 'id' },
        { data: 'name' },
        { data: 'address' },
        { data: 'acreage' },
        { data: 'constructArea' },
        { data: 'numBlock' },
        { data: 'numApartment' },
        { data: 'investor' },
        { data: 'constructionContractor' },
        { data: 'designUnit' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 0, // Cột số thứ tự là 0 (cột STT)
            render: function () {
                return gSTT++;
            }
        },
        {
            targets: -1,
            defaultContent: '<i class="fas fa-edit mr-3 btn-update"></i>' + '<i class="fas fa-trash-alt btn-delete"></i>'
        }
    ]
}).buttons().container().appendTo('#project-table_wrapper .col-md-6:eq(0)');
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    $("#btn-add").on("click", function () {
        $("#modal-add").modal('show');
        $("#btn-save-add").on("click", function () {
            onBtnAdd();
        })
    })

    $("#project-table").on("click", ".btn-update", function () {
        $("#modal-up").modal('show');
        var vData = getDataRowTable(this);
        fillDataUpdate(vData);
        $("#btn-save-up").on("click", function () {
            onBtnUp(vData.id);
        })
    })
    $("#project-table").on("click", ".btn-delete", function () {
        $("#modal-delete").modal('show');
        var vData = getDataRowTable(this);
        $("#btn-save-delete").on("click", function () {
            onBtnDelete(vData.id);
        })
    })
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    $.ajax({
        url: gBASE_URL + "projects",
        type: "GET",
        success: function (paramRes) {
            drawTable(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
function onBtnAdd() {
    var vObj = {
        name: "",
        address: "",
        acreage: "",
        constructArea: "",
        numBlock: "",
        numApartment: "",
        investor: "",
        constructionContractor: "",
        designUnit: ""
    }
    getInfo(vObj);
    var vCheck = validateInfo(vObj);
    if (vCheck) {
        $.ajax({
            url: gBASE_URL + "projects",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(vObj),
            success: function (paramRes) {
                alert("Tạo mới dữ liệu thành công");
                location.reload();
            },
            error: function (paramErr) {
                alert(paramErr.status);
            }
        })
    }
}
function onBtnUp(paramId) {
    var vObj = {
        name: "",
        address: "",
        acreage: "",
        constructArea: "",
        numBlock: "",
        numApartment: "",
        investor: "",
        constructionContractor: "",
        designUnit: ""
    }
    getInfoUpdate(vObj);
    $.ajax({
        url: gBASE_URL + "projects/" + paramId,
        type: "PUT",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(vObj),
        success: function (paramRes) {
            alert("Cập nhật dữ liệu thành công!");
            location.reload();
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}

function onBtnDelete(paramId) {
    $.ajax({
        url: gBASE_URL + "projects/" + paramId,
        type: "DELETE",
        success: function (paramRes) {
            alert("Xóa dữ liệu thành công");
            location.reload();
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function drawTable(paramData) {
    gSTT = 1;
    var vTable = $("#project-table").DataTable();
    vTable.clear();
    vTable.rows.add(paramData);
    vTable.draw();
}

function getInfo(paramObj) {
    paramObj.name = $("#inp-add-name").val();
    paramObj.address = $("#inp-add-address").val();
    paramObj.acreage = $("#inp-add-acreage").val();
    paramObj.constructArea = $("#inp-add-constructArea").val();
    paramObj.numBlock = $("#inp-add-numBlock").val();
    paramObj.numApartment = $("#inp-add-numApartment").val();
    paramObj.investor = $("#inp-add-investor").val();
    paramObj.constructionContractor = $("#inp-add-constructionContractor").val();
    paramObj.designUnit = $("#inp-add-designUnit").val();
}

function validateInfo(paramObj) {
    if (paramObj.name == "") {
        alert("Xin hãy nhập tên dự án!");
        return false;
    } else if (paramObj.address == "") {
        alert("Xin hãy nhập địa chỉ!");
        return false;
    } else if (paramObj.acreage == "") {
        alert("Xin hãy nhập tổng diện tích dự án!");
        return false;
    } else if (paramObj.constructArea == "") {
        alert("Xin hãy nhập diện tích xây dựng!");
        return false;
    } else if (paramObj.numBlock == "") {
        alert("Xin hãy nhập số tòa nhà");
        return false;
    } else if (paramObj.numApartment == "") {
        alert("Xin hãy nhập số căn hộ!");
        return false;
    } else if (paramObj.investor == "") {
        alert("Xin hãy nhập chủ đầu tư!");
        return false;
    } else if (paramObj.constructionContractor == "") {
        alert("Xin hãy nhập nhà thầu xây dựng!");
        return false;
    } else if (paramObj.designUnit == "") {
        alert("Xin hãy nhập đơn vị thiết kế!");
        return false;
    }
    return true;
}

function getDataRowTable(paramElement) {
    var vTable = $("#project-table").DataTable();
    var vRow = $(paramElement).closest("tr");
    var vData = vTable.row(vRow).data();
    return vData;
}
function fillDataUpdate(paramData) {
    $("#inp-update-name").val(paramData.name);
    $("#inp-update-address").val(paramData.address);
    $("#inp-update-acreage").val(paramData.acreage);
    $("#inp-update-constructArea").val(paramData.constructArea);
    $("#inp-update-numBlock").val(paramData.numBlock);
    $("#inp-update-numApartment").val(paramData.numApartment);
    $("#inp-update-investor").val(paramData.investor);
    $("#inp-update-constructionContractor").val(paramData.constructionContractor);
    $("#inp-update-designUnit").val(paramData.designUnit);
}
function getInfoUpdate(paramObj) {
    paramObj.name = $("#inp-update-name").val();
    paramObj.address = $("#inp-update-address").val();
    paramObj.acreage = $("#inp-update-acreage").val();
    paramObj.constructArea = $("#inp-update-constructArea").val();
    paramObj.numBlock = $("#inp-update-numBlock").val();
    paramObj.numApartment = $("#inp-update-numApartment").val();
    paramObj.investor = $("#inp-update-investor").val();
    paramObj.constructionContractor = $("#inp-update-constructionContractor").val();
    paramObj.designUnit = $("#inp-update-designUnit").val();
}
