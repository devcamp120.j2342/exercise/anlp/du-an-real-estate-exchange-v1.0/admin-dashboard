"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */


/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    //Kiểm tra token nếu có token tức người dùng đã đăng nhập

    const token = getCookie("token");

    if (token) {
        window.location.href = "http://127.0.0.1:80/Admin_Dashboard/RealEstate.html";
    }
    $("#login-btn").on("click", function (event) {
        event.preventDefault();
        signInClick()
    });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function signInClick() {
    var vLogin = {
        username: "",
        password: ""
    }
    vLogin.username = $("#input-username").val().trim();
    vLogin.password = $("#input-password").val().trim();
    var isValidated = validateData(vLogin)
    if (isValidated) {
        $.ajax({
            url: "http://localhost:8080/api/auth/signin",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(vLogin),
            success: function (res) {  
                // Set the cookie with the root domain
                document.cookie = "username=" + res.username  + ";path=/Admin_Dashboard";
                setCookie("token", res.accessToken, 1);

                if (res.roles == "ROLE_USER") {
                    // Nếu là người dùng bình thường thì không có quyền sử dụng admin
                    alert("Bạn không có quyền sử dụng trang web này!")
                } else {
                    // Redirect to "contractor.html" for users without "ROLE_DEFAULT" role
                    window.location.href = "http://127.0.0.1:80/Admin_Dashboard/RealEstate.html";
                }
            },
            error: function () {     
                alert("Tài khoản hoặc mật khẩu không đúng")
            }
        });
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/



    //Hàm setCookie
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/Admin_Dashboard";
    }

    //Hàm get Cookie
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }





//Validate dữ liệu từ
function validateData(vData) {
    if (vData.username === "") {
        alert("Bạn chưa nhập username");
        return false;
    };

    if (vData.password === "") {
        alert("Bạn chưa nhập password");
        return false;
    }
    return true;
}