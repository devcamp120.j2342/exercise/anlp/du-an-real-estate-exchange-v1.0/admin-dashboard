"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gSTT = 1;
const gBASE_URL = "http://localhost:8080/";
var gAddressTable = $("#customer-table").DataTable({
    "responsive": true, "lengthChange": false, "autoWidth": false,
    "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
    "scrollX": true,
    "columns": [
        { data: 'id' },
        { data: 'contactTitle' },
        { data: 'contactName' },
        { data: 'address' },
        { data: 'mobile' },
        { data: 'email' },
        { data: 'createDate' },
        { data: 'updateDate' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 0, // Cột số thứ tự là 0 (cột STT)
            render: function() {
              return gSTT++;
            }
          },
        {
            targets: -1,
            defaultContent: '<i class="fas fa-edit mr-3 btn-update"></i>' + '<i class="fas fa-trash-alt btn-delete"></i>'
        }
    ]
}).buttons().container().appendTo('#customer-table_wrapper .col-md-6:eq(0)');
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    $("#btn-add").on("click", function () {
        $("#modal-add").modal('show');
        $("#btn-save-add").on("click", function () {
            onBtnAdd();
        })
    })

    $("#customer-table").on("click", ".btn-update", function () {
        $("#modal-up").modal('show');
        var vData = getDataRowTable(this);
        fillDataUpdate(vData);
        $("#btn-save-up").on("click", function () {
            onBtnUp(vData.id);
        })
    })
    $("#customer-table").on("click", ".btn-delete", function () {
        $("#modal-delete").modal('show');
        var vData = getDataRowTable(this);
        $("#btn-save-delete").on("click", function () {
            onBtnDelete(vData.id);
        })
    })
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    $.ajax({
        url: gBASE_URL + "customers",
        type: "GET",
        success: function (paramRes) {
            drawTable(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
function onBtnAdd() {
    var vObj = {
        contactTitle: "",
        contactName: "",
        address: "",
        mobile: "",
        email: "",
    }
    getInfo(vObj);
    var vCheck = validateInfo(vObj);
    if (vCheck) {
        $.ajax({
            url: gBASE_URL + "customers",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(vObj),
            success: function (paramRes) {
                alert("Tạo mới dữ liệu thành công");
                location.reload();
            },
            error: function (paramErr) {
                alert(paramErr.status);
            }
        })
    }
}
function onBtnUp(paramId) {
    var vObj = {
        contactTitle: "",
        contactName: "",
        address: "",
        mobile: "",
        email: "",
    }
    getInfoUpdate(vObj);
    $.ajax({
        url: gBASE_URL + "customers/" + paramId,
        type: "PUT",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(vObj),
        success: function (paramRes) {
            alert("Cập nhật dữ liệu thành công!");
            location.reload();
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}

function onBtnDelete(paramId) {
    $.ajax({
        url: gBASE_URL + "customers/" + paramId,
        type: "DELETE",
        success: function (paramRes) {
            alert("Xóa dữ liệu thành công");
            location.reload();
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function drawTable(paramData) {
    gSTT = 1;
    var vTable = $("#customer-table").DataTable();
    vTable.clear();
    vTable.rows.add(paramData);
    vTable.draw();
}

function getInfo(paramObj) {
    paramObj.contactTitle = $("#inp-add-contactTitle").val();
    paramObj.contactName = $("#inp-add-contactName").val();
    paramObj.address = $("#inp-add-address").val();
    paramObj.mobile = $("#inp-add-address").val();
    paramObj.email = $("#inp-add-address").val();
}

function validateInfo(paramObj) {
    if (paramObj.contactName == "" ){
        alert("Xin hãy nhập tên khách hàng!");
        return false;
    } else if (paramObj.address == ""){
        alert("Xin hãy nhập địa chỉ!");
        return false;
    } else if (paramObj.mobile == ""){
        alert("Xin hãy nhập số điện thoại!");
        return false;
    } else if (paramObj.email == ""){
        alert("Xin hãy nhập email!");
        return false;
    }
    return true;
}

function getDataRowTable(paramElement) {
    var vTable = $("#customer-table").DataTable();
    var vRow = $(paramElement).closest("tr");
    var vData = vTable.row(vRow).data();
    return vData;
}
function fillDataUpdate(paramData) {
    $("#inp-update-contactTitle").val(paramData.contactTitle);
    $("#inp-update-contactName").val(paramData.contactName);
    $("#inp-update-address").val(paramData.address);
    $("#inp-update-mobile").val(paramData.mobile);
    $("#inp-update-email").val(paramData.email);
}
function getInfoUpdate(paramObj) {
    paramObj.contactTitle = $("#inp-update-contactTitle").val();
    paramObj.contactName = $("#inp-update-contactName").val();
    paramObj.address = $("#inp-update-address").val();
    paramObj.mobile = $("#inp-update-mobile").val();
    paramObj.email = $("#inp-update-email").val();
}
