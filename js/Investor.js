"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gSTT = 1;
const gBASE_URL = "http://localhost:8080/";
var gAddressTable = $("#investor-table").DataTable({
    "responsive": true, "lengthChange": false, "autoWidth": false,
    "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
    "scrollX": true,
    "columns": [
        { data: 'id' },
        { data: 'name' },
        { data: 'address' },
        { data: 'phone' },
        { data: 'phone2' },
        { data: 'fax' },
        { data: 'email' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 0, // Cột số thứ tự là 0 (cột STT)
            render: function() {
              return gSTT++;
            }
          },
        {
            targets: -1,
            defaultContent: '<i class="fas fa-edit mr-3 btn-update"></i>' + '<i class="fas fa-trash-alt btn-delete"></i>'
        }
    ]
}).buttons().container().appendTo('#investor-table_wrapper .col-md-6:eq(0)');
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    $("#btn-add").on("click", function () {
        $("#modal-add").modal('show');
        $("#btn-save-add").on("click", function () {
            onBtnAdd();
        })
    })

    $("#investor-table").on("click", ".btn-update", function () {
        $("#modal-up").modal('show');
        var vData = getDataRowTable(this);
        fillDataUpdate(vData);
        $("#btn-save-up").on("click", function () {
            onBtnUp(vData.id);
        })
    })
    $("#investor-table").on("click", ".btn-delete", function () {
        $("#modal-delete").modal('show');
        var vData = getDataRowTable(this);
        $("#btn-save-delete").on("click", function () {
            onBtnDelete(vData.id);
        })
    })
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    $.ajax({
        url: gBASE_URL + "investors",
        type: "GET",
        success: function (paramRes) {
            drawTable(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
function onBtnAdd() {
    var vObj = {
        name: "",
        phone: "",
        phone2: "",
        fax: "",
        email: "",
    }
    getInfo(vObj);
    var vCheck = validateInfo(vObj);
    if (vCheck) {
        $.ajax({
            url: gBASE_URL + "investors",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(vObj),
            success: function () {
                alert("Tạo mới dữ liệu thành công");
                location.reload();
            },
            error: function (paramErr) {
                alert(paramErr.status);
            }
        })
    }
}
function onBtnUp(paramId) {
    var vObj = {
        name: "",
        phone: "",
        phone2: "",
        fax: "",
        email: "",
    }
    getInfoUpdate(vObj);
    $.ajax({
        url: gBASE_URL + "investors/" + paramId,
        type: "PUT",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(vObj),
        success: function (paramRes) {
            alert("Cập nhật dữ liệu thành công!");
            location.reload();
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}

function onBtnDelete(paramId) {
    $.ajax({
        url: gBASE_URL + "investors/" + paramId,
        type: "DELETE",
        success: function () {
            alert("Xóa dữ liệu thành công");
            location.reload();
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function drawTable(paramData) {
    gSTT = 1;
    var vTable = $("#investor-table").DataTable();
    vTable.clear();
    vTable.rows.add(paramData);
    vTable.draw();
}

function getInfo(paramObj) {
    paramObj.name = $("#inp-add-name").val();
    paramObj.address = $("#inp-add-address").val();
    paramObj.phone = $("#inp-add-phone").val();
    paramObj.phone2 = $("#inp-add-phone2").val();
    paramObj.fax = $("#inp-add-fax").val();
    paramObj.email = $("#inp-add-email").val();
}

function validateInfo(paramObj) {
    if (paramObj.name == "" ){
        alert("Xin hãy nhập tên đơn vị thiết kế!");
        return false;
    } else if (paramObj.address == ""){
        alert("Xin hãy nhập đại chỉ trụ sở chính!");
        return false;
    } else if (paramObj.phone == ""){
        alert("Xin hãy nhập số điện thoại!");
        return false;
    } else if (paramObj.fax == ""){
        alert("Xin hãy nhập fax!");
        return false;
    } else if (paramObj.email == ""){
        alert("Xin hãy nhập email!");
        return false;
    }
    return true;
}

function getDataRowTable(paramElement) {
    var vTable = $("#investor-table").DataTable();
    var vRow = $(paramElement).closest("tr");
    var vData = vTable.row(vRow).data();
    return vData;
}
function fillDataUpdate(paramData) {
    $("#inp-update-name").val(paramData.name);
    $("#inp-update-address").val(paramData.address);
    $("#inp-update-phone").val(paramData.phone);
    $("#inp-update-phone2").val(paramData.phone2);
    $("#inp-update-fax").val(paramData.fax);
    $("#inp-update-email").val(paramData.email);

}
function getInfoUpdate(paramObj) {
    paramObj.name = $("#inp-update-name").val();
    paramObj.address = $("#inp-update-address").val();
    paramObj.phone = $("#inp-update-phone").val();
    paramObj.phone2 = $("#inp-update-phone2").val();
    paramObj.fax = $("#inp-update-fax").val();
    paramObj.email = $("#inp-update-email").val();
}
